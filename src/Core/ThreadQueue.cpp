#include "ThreadQueue.h"

ThreadQueue::ThreadQueue()
{
	head = tail = nullptr;
}

void ThreadQueue::push(IThreadTask * op, int priority)
{
	task * t = new task;
	t->next = nullptr;
	t->priority = priority;
	t->op = op;
	if (tail == nullptr)
	{
		head = tail = t;
		return;
	}
	if (op->operator==(*head->op))
		return;
	for (task * i = head->next, *prev = head; i != nullptr; prev = i, i = i->next)
	{
		if (!op->operator==(*i->op))
		{
			if (i->priority > priority)
			{
				t->next = i;
				prev->next = t;
				return;
			}
		}
		else
			return;
	}
	tail->next = t;
	tail = t;
}

IThreadTask & ThreadQueue::peek()
{
	return *head->op;
}

void ThreadQueue::pop()
{
	if (head != nullptr)
	{
		task * tmp = head;
		if (tail == head)
			tail = nullptr;
		head = head->next;
		delete tmp->op;
		delete tmp;
	}
}

int ThreadQueue::contains(IThreadTask * op)
{
	for (task * tmp = head; tmp != nullptr; tmp = tmp->next)
		if (*tmp->op == *op)
			return tmp->priority;
	return -1;
}

void ThreadQueue::invoke(void * param)
{
	if (head != nullptr)
	{
		IThreadTask & tmp = peek();
		tmp.operation(param);
		pop();
	}
}