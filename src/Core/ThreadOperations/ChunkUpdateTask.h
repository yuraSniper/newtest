#include "IThreadTask.h"
#include <Test.h>

class ChunkUpdateTask : public IThreadTask
{
	short x, z, mesh;
public:
	ChunkUpdateTask(short nx, short nz, short nmesh) : x(nx), z(nz),
		mesh(nmesh), IThreadTask(2)
	{
	}

	void operation(void * param)
	{
		Chunk * chunk = Test::getInstance().world->getChunk(x, z);
		if (chunk != nullptr)
			chunk->updateMesh(mesh);
	}

	bool operator==(IThreadTask & t)
	{
		return t.type == type && ((ChunkUpdateTask *)(void *)&t)->x == x &&
			((ChunkUpdateTask *)(void *)&t)->z == z && ((ChunkUpdateTask *)(void *)&t)->mesh == mesh;
	}
};