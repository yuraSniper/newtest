#pragma once

class IThreadTask
{
protected:
	IThreadTask(int t) : type(t)
	{
	}
public:
	int type;
	virtual void operation(void * param) = 0;
	virtual bool operator ==(IThreadTask & o)
	{
		return type == o.type;
	}
};