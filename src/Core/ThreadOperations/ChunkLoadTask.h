#include "IThreadTask.h"
#include <Test.h>

class ChunkLoadTask : public IThreadTask
{
	short x, z;
public:
	ChunkLoadTask(short nx, short nz) : x(nx), z(nz), IThreadTask(1)
	{
	}

	void operation(void * param)
	{
		Test::getInstance().world->loadChunk(x, z);
	}

	bool operator==(IThreadTask & t)
	{
		return t.type == type && ((ChunkLoadTask *)(void *)&t)->x == x &&
			((ChunkLoadTask *)(void *)&t)->z == z;
	}
};