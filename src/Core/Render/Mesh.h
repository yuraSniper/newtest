#pragma once

#include <Util\std.h>
#include "Vertex.h"

class Mesh
{
	CRITICAL_SECTION crit;
	vector<Vertex> mesh;
	unsigned int vboId;
public:
	float xOffset, yOffset, zOffset;
	bool updated;
	bool updating;
	bool updateScheduled;
	bool generated;

	Mesh();
	void addVertex(Vertex * vertArr, int count = 1);
	void draw(bool useTex = false);
	void clear();
};