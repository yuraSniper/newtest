#include "Mesh.h"

Mesh::Mesh()
{
	updateScheduled = false;
	updating = false;
	vboId = 0;
	xOffset = yOffset = zOffset = 0;
	generated = false;
}

void Mesh::addVertex(Vertex * vertArr, int count)
{
	for (int i = 0; i < count; i++)
		mesh.push_back(Vertex(vertArr[i].x + xOffset, vertArr[i].y + yOffset,
			vertArr[i].z + zOffset, vertArr[i].u, vertArr[i].v, vertArr[i].color));
	updated = true;
}

void Mesh::draw(bool useTex)
{
	if (updating || mesh.empty())
		return;

	if (vboId == 0)
		glGenBuffers(1, &vboId);

	glBindBuffer(GL_ARRAY_BUFFER, vboId);
	if (updated)
	{
		updated = false;
		glBufferData(GL_ARRAY_BUFFER, mesh.size() * sizeof(Vertex), &(mesh[0].x), GL_DYNAMIC_DRAW);
	}
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), nullptr);
	glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Vertex), (void *)(sizeof(float) * 5));

	if (useTex)
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), (void *)(sizeof(float) * 3));
	}

	glDrawArrays(GL_QUADS, 0, mesh.size());

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	if (useTex)
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void Mesh::clear()
{
	mesh.clear();
}