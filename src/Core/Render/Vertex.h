#pragma once

struct Vertex
{
	float x, y, z;
	float u, v;
	int color;

	Vertex(float nx, float ny, float nz, float nu, float nv)
	{
		x = nx;
		y = ny;
		z = nz;
		u = nu;
		v = nv;
		color = 0xffffffff;
	}

	Vertex(float nx, float ny, float nz, int ncolor)
	{
		x = nx;
		y = ny;
		z = nz;
		u = v = 0;
		color = ncolor;
	}

	Vertex(float nx, float ny, float nz, float nu, float nv, int ncolor)
	{
		x = nx;
		y = ny;
		z = nz;
		u = nu;
		v = nv;
		color = ncolor;
	}
};