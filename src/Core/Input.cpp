#include "Input.h"

void Input::onKey(void * param, void * key)
{
	char * k = (char *)key;
	for (short i = 0; i < 256; i++)
	{
		((Input *)param)->keys[i] = (k[i] >> 7);
		if (((Input *)param)->keys[i])
			((Input *)param)->keysPressed[i] = true;
	}
}

void Input::onMouseMove(void * param, void * pos)
{
	((Input *)param)->mx = ((short *)pos)[0];
	((Input *)param)->my = ((short *)pos)[1];
}

void Input::onMouseDown(void * param, void * b)
{
	((Input *)param)->mouse[*(int *)b] = true;
}

void Input::onMouseUp(void * param, void * b)
{
	((Input *)param)->mouse[*(int *)b] = false;
}

Input::Input(Window * w)
{
	wnd = w;
	wnd->onKey.add(onKey, this);
	wnd->onMouseMove.add(onMouseMove, this);
	wnd->onMouseDown.add(onMouseDown, this);
	wnd->onMouseUp.add(onMouseUp, this);

	mx = my = 0;

	for (short i = 0; i < 3; i++)
		mouse[i] = false;

	for (short i = 0; i < 256; i++)
		keys[i] = keysPressed[i] = false;
}

bool Input::getKeyPressed(unsigned char key)
{
	if (!keys[key] && keysPressed[key])
	{
		keysPressed[key] = false;
		return true;
	}
	return false;
}

bool Input::getKey(unsigned char key)
{
	return keys[key];
}

int Input::getMousePos(short coord)
{
	return coord == 0 ? mx : my;
}

bool Input::getMouse(short button)
{
	return mouse[button];
}