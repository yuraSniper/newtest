#pragma once

#include <Util\std.h>
#include <Core\ThreadQueue.h>

enum THREADTYPE
{
	UNKNOWN = -1,
	RENDERTHREAD
};

class IThread
{
	static int __stdcall callbackProc(void * param)
	{
		return ((IThread*)param)->threadProc();
	}
	HANDLE hThread;
protected:
	virtual int threadProc() = 0;
public:
	THREADTYPE threadType;
	ThreadQueue * threadQueue;
	IThread()
	{
		threadType = UNKNOWN;
		threadQueue = new ThreadQueue();
		hThread = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)callbackProc, this, CREATE_SUSPENDED, 0);
	}
	void pause()
	{
		SuspendThread(hThread);
	}

	void resume()
	{
		ResumeThread(hThread);
	}

	void terminate()
	{
		TerminateThread(hThread, 0);
	}
};