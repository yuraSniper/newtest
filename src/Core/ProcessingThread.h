#pragma once

#include "IThread.h"

class ProcessingThread : public IThread
{
	int threadProc()
	{
		while (true)
		{
			threadQueue->invoke(nullptr);
			Sleep(0);
		}
		return 0;
	}
};