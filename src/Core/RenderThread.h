#pragma once

#include "ThreadQueue.h"
#include "IThread.h"

class RenderThread : public IThread
{
public:
	float fps;
private:
	HGLRC hRC;
	int threadProc()
	{
		threadType = RENDERTHREAD;
		Window * wnd = Test::getInstance().mainWnd;

		hRC = wglCreateContext(wnd->getHDC());
		wglMakeCurrent(wnd->getHDC(), hRC);

		glewInit();

		glClearColor(0, 0, 0, 0);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glClearDepth(1);

		glEnable(GL_CULL_FACE);

		//glEnable(GL_ALPHA);
		//glAlphaFunc(GL_LEQUAL, 0);
		
		//glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		World * wld = Test::getInstance().world;
		EntityPlayer * player = Test::getInstance().player;
		int w, h;
		int dt;

		while (true)
		{
			dt = GetTickCount();
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			w = wnd->getWidth();
			h = wnd->getHeight();
			glViewport(0, 0, w, h);

			//World
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();

			h = (h == 0)? 1 : h;

			gluPerspective(45, (double)(w) / (h), 0.01, 1000);

			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			if (wld != nullptr)
			{
				glRotated(-player->lookAngles.x, 1, 0, 0);
				glRotated(-player->lookAngles.y, 0, 1, 0);
				glTranslated(-player->relPos.x, -player->relPos.y, -player->relPos.z);
				glTranslated(player->eyeOffset.x, player->eyeOffset.y, player->eyeOffset.z);

				wld->renderFromPlayerPos();
			}

			/*glBegin(GL_LINES);
			glColor3f(1, 0, 0);
			glVertex3f(0, 0, 0);
			glVertex3f(1, 0, 0);

			glColor3f(0, 1, 0);
			glVertex3f(0, 0, 0);
			glVertex3f(0, 1, 0);

			glColor3f(0, 0, 1);
			glVertex3f(0, 0, 0);
			glVertex3f(0, 0, 1);
			glEnd();*/

			//~World
			//GUI/HUD

			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();

			glOrtho(0, w, h, 0, 0, 100);

			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			glBegin(GL_LINES);

			glColor3ub(255, 0, 0);
			glVertex2f(0, 0);
			glColor3ub(0, 255, 0);
			glVertex2f(w, h);
			glEnd();

			//~GUI/HUD

			SwapBuffers(wnd->getHDC());

			dt = GetTickCount() - dt;
			fps = 1. / dt;
			Sleep(0);
		}

		return 0;
	}
};