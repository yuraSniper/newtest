#include "Window.h"
#include "Test.h"

void Window::registerWndClass(LPWSTR cls)
{
	WNDCLASS wc = {0};
	wc.hbrBackground = CreateSolidBrush(0x00000000);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hInstance = hInst = GetModuleHandle(nullptr);
	wc.lpfnWndProc = WndProc;
	wc.lpszClassName = cls;
	wc.style = CS_CLASSDC | CS_VREDRAW | CS_HREDRAW;

	RegisterClass(&wc);
}

Window::Window(int width, int height)
{
	RECT rt;
	GetWindowRect(GetDesktopWindow(), &rt);
	rt.right -= rt.left;
	rt.bottom -= rt.top;

	registerWndClass(L"Test");
	hWnd = CreateWindowA("Test", "", WS_OVERLAPPEDWINDOW, 
		(rt.right - width) / 2, (rt.bottom - height) / 2,
		width, height, nullptr, nullptr, hInst, nullptr);
	SetPropA(hWnd, "This", this);
	hDC = GetDC(hWnd);
	PIXELFORMATDESCRIPTOR pfd = {0};
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL;

	SetPixelFormat(hDC, ChoosePixelFormat(hDC, &pfd), &pfd);
}

HWND Window::getHWND()
{
	return hWnd;
}

HDC Window::getHDC()
{
	return hDC;
}

void Window::setTitle(char * title)
{
	SetWindowTextA(hWnd, title);
}

void Window::setVisibility(bool show)
{
	ShowWindow(hWnd, show? SW_SHOW : SW_HIDE);
}

void Window::setCursorVisibility(bool show)
{
	ShowCursor(show? 1 : 0);
}

LRESULT __stdcall Window::WndProc(HWND hWnd, unsigned int msg, WPARAM wParam, LPARAM lParam)
{
	int button;
	Window & wnd = *(Window *)GetPropA(hWnd, "This");
	switch (msg)
	{
		case WM_KEYDOWN:
		case WM_KEYUP:
			unsigned char keys[256];
			GetKeyboardState(keys);
			wnd.onKey(keys);
			return 0;
		case WM_MOUSEMOVE:
			wnd.onMouseMove(&lParam);
			return 0;
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
		case WM_MBUTTONDOWN:
			button = -(signed)(WM_LBUTTONDOWN - msg) / 3;
			wnd.onMouseDown(&button);
			return 0;
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
		case WM_MBUTTONUP:
			button = -(signed)(WM_LBUTTONUP - msg) / 3;
			wnd.onMouseUp(&button);
			return 0;
		case WM_ERASEBKGND:
			return 0;
		case WM_CLOSE:
		case WM_DESTROY:
			Test::getInstance().shutdown();
			return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

int Window::getHeight()
{
	RECT rt;
	GetClientRect(hWnd, &rt);
	return rt.bottom - rt.top;
}

int Window::getWidth()
{
	RECT rt;
	GetClientRect(hWnd, &rt);
	return rt.right - rt.left;
}

bool Window::isVisible()
{
	WINDOWPLACEMENT wp;
	wp.length = sizeof(wp);
	GetWindowPlacement(hWnd, &wp);
	return wp.showCmd != SW_HIDE && wp.showCmd != SW_MINIMIZE;
}

bool Window::isActive()
{
	return IsWindowEnabled(hWnd) != 0;
}