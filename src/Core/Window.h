#pragma once

#include <Util\std.h>
#include <Util\Delegate.h>

class Test;

class Window
{
	HWND hWnd;
	HINSTANCE hInst;
	HDC hDC;
	void registerWndClass(LPWSTR cls);
	static LRESULT __stdcall WndProc(HWND hWnd, unsigned int msg, WPARAM wParam, LPARAM lParam);
public:
	Delegate onKey, onMouseMove, onMouseDown, onMouseUp;

	Window(int width, int height);
	HWND getHWND();
	HDC getHDC();
	void setTitle(char * title);
	void setVisibility(bool show = true);
	void setCursorVisibility(bool show = true);
	int getHeight();
	int getWidth();
	bool isVisible();
	bool isActive();
};