#pragma once

#include <Core\Window.h>

class Input
{
	Window * wnd;
	bool keys[256], keysPressed[256];
	bool mouse[3];
	int mx, my;

	static void onKey(void * param, void * key);
	static void onMouseMove(void * param, void * pos);
	static void onMouseDown(void * param, void * b);
	static void onMouseUp(void * param, void * b);
public:
	Input(Window * w);

	bool getKey(unsigned char key);
	bool getKeyPressed(unsigned char key);
	int getMousePos(short coord);
	bool getMouse(short button);
};