#pragma once

#include <Core\ThreadOperations\IThreadTask.h>

class ThreadQueue
{
	struct task
	{
		task * next;
		int priority;
		IThreadTask * op;
	};
	task * head;
	task * tail;
public:
	ThreadQueue();
	void push(IThreadTask * op, int priority);
	IThreadTask & peek();
	void pop();
	int contains(IThreadTask * op);
	void invoke(void * param);
};