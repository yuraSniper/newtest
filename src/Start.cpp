#include <Util\std.h>
#include "Test.h"

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "Glu32.lib")

int __stdcall WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Test::getInstance();
	return 0;
}