#include "Test.h"
#include <Core\RenderThread.h>
#include <Core\ProcessingThread.h>

Test & Test::getInstance()
{
	static Test instance;
	return instance;
}

Test::Test()
{
	mainWnd = new Window(800, 600);
	mainWnd->setTitle("newTest");
	mainWnd->setVisibility(true);

	trapMouse = false;

	input = new Input(mainWnd);

	player = new EntityPlayer();
	player->setPosition(0, 0, 8, 200, 8);
	player->setEyeOffset(0, 0, 0);
	player->lookAngles.y = 0;
	player->lookAngles.x = -89;

	world = new World();

	renderThread = new RenderThread();
	miscThread = new ProcessingThread();
	mainLoop();
}

void Test::mainLoop()
{
	MSG msg;
	working = true;
	renderThread->resume();
	miscThread->resume();
	while (working)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		handleInput();
		if (trapMouse != prevMouse)
		{
			ShowCursor(!trapMouse);
			prevMouse = trapMouse;
		}
		Sleep(1);
	}
}

void Test::shutdown()
{
	renderThread->terminate();
	miscThread->terminate();
	working = false;
}

void Test::handleInput()
{
	static int prevX = input->getMousePos(0), prevY = input->getMousePos(1);
	static double k = 0.08;
	int x = input->getMousePos(0), y = input->getMousePos(1);

	if (input->getKey('W'))
	{
		player->relPos.x -= sin(deg2rad(player->lookAngles.y)) * k;
		player->relPos.z -= cos(deg2rad(player->lookAngles.y)) * k;
	}

	if (input->getKey('S'))
	{
		player->relPos.x += sin(deg2rad(player->lookAngles.y)) * k;
		player->relPos.z += cos(deg2rad(player->lookAngles.y)) * k;
	}

	if (input->getKey('A'))
	{
		player->relPos.x -= sin(deg2rad(player->lookAngles.y + 90)) * k * 0.6;
		player->relPos.z -= cos(deg2rad(player->lookAngles.y + 90)) * k * 0.6;
	}

	if (input->getKey('D'))
	{
		player->relPos.x += sin(deg2rad(player->lookAngles.y + 90)) * k * 0.6;
		player->relPos.z += cos(deg2rad(player->lookAngles.y + 90)) * k * 0.6;
	}

	if (abs(player->relPos.x) >= Chunk::size)
	{
		int d = ((int)player->relPos.x) / Chunk::size;
		player->relPos.x -= d * Chunk::size;
		player->chunkPos.x += d;
	}

	if (abs(player->relPos.z) >= Chunk::size)
	{
		int d = ((int)player->relPos.z) / Chunk::size;
		player->relPos.z -= d * Chunk::size;
		player->chunkPos.z += d;
	}

	if (input->getKey(VK_LSHIFT))
	{
		player->relPos.y -= k;
	}

	if (input->getKey(VK_SPACE))
	{
		player->relPos.y += k;
	}

	if (input->getKeyPressed(VK_F4))
	{
		trapMouse = !trapMouse;
		prevX = x;
		prevY = y;
	}

	if (trapMouse)
	{
		RECT rt, rt2;
		POINT pt;
		GetClientRect(mainWnd->getHWND(), &rt);
		pt.x = rt.right - 1;
		pt.y = rt.bottom - 1;
		ClientToScreen(mainWnd->getHWND(), &pt);
		rt2.right = pt.x;
		rt2.bottom = pt.y;
		pt.x = pt.y = 0;
		ClientToScreen(mainWnd->getHWND(), &pt);
		rt2.left = pt.x;
		rt2.top = pt.y;
		ClipCursor(&rt2);

		if (abs(x - rt.left) < 10 || abs(x - rt.right) < 10 ||
			abs(y - rt.top) < 10 || abs(y - rt.bottom) < 10)
		{
			POINT p = {rt.left + (rt.right - rt.left) / 2, rt.top + (rt.bottom - rt.top) / 2};
			x = prevX = p.x;
			y = prevY = p.y;
			ClientToScreen(mainWnd->getHWND(), &p);
			SetCursorPos(p.x, p.y);
		}

		if (x != prevX || y != prevY)
		{
			renderThread->pause();
			player->lookAngles.x -= (y - prevY) * 0.5;
			player->lookAngles.y -= (x - prevX) * 0.5;

			if (player->lookAngles.x > 89)
				player->lookAngles.x = 89;
			if (player->lookAngles.x < -89)
				player->lookAngles.x = -89;
			if (player->lookAngles.y > 360)
				player->lookAngles.y -= 360;
			if (player->lookAngles.y < 0)
				player->lookAngles.y = 360 + player->lookAngles.y;

			renderThread->resume();

			prevX = x;
			prevY = y;
		}
	}
	else
	{
		ClipCursor(nullptr);
	}
}