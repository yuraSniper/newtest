#pragma once

#include "std.h"

inline double deg2rad(double a)
{
	return a * M_PI / 180.;
}