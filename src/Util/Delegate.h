#pragma once

#include "std.h"

class Delegate
{
	struct Func
	{
		void (*f)(void *, void *);
		void * param;
		Func(void (*f)(void *, void *), void * p)
		{
			this->f = f;
			this->param = p;
		}
	};
	vector<Func> list;

public:
	void add(void (*f)(void *, void *), void * param)
	{
		for (auto i = list.begin(); i != list.end(); i++)
			if (i->f == f && i->param == param)
				return ;
		list.push_back(Func(f, param));
	}

	void remove(void (*f)(void *, void *), void * param)
	{
		for (auto i = list.begin(); i != list.end(); i++)
			if (i->f == f && i->param == param)
			{
				list.erase(i);
				return ;
			}
	}

	void operator()(void * prm)
	{
		for (auto i = list.begin(); i != list.end(); i++)
			(i->f)(i->param, prm);
	}
};