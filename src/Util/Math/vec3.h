#pragma once

class vec3
{
public:
	double x, y, z;
	vec3(double ax = 0, double ay = 0, double az = 0);
	vec3 operator+(vec3 & v);
	vec3 operator-(vec3 & v);
	vec3 operator-();
	vec3 operator*(double k);
	double dot(vec3 & v);
	vec3 cross(vec3 & v);
	double lenSq();
	double len();

	static vec3 vector3FromAngles(double yaw, double pitch);
};