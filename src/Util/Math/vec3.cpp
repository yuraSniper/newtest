#include "vec3.h"
#include <Util\std.h>
#include <Util\Util.h>

vec3::vec3(double ax, double ay, double az)
{
	x = ax;
	y = ay;
	z = az;
}

vec3 vec3::operator+(vec3 & v)
{
	return vec3(x + v.x, y + v.y, z + v.z);
}

vec3 vec3::operator-()
{
	return vec3(-x, -y, -z);
}

vec3 vec3::operator-(vec3 & v)
{
	return vec3(x - v.x, y - v.y, z - v.z);
}

vec3 vec3::operator*(double k)
{
	return vec3(x * k, y * k, z * k);
}

double vec3::dot(vec3 & v)
{
	return x * v.x + y * v.y + z * v.z;
}

vec3 vec3::cross(vec3 & v)
{
	return vec3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
}

double vec3::lenSq()
{
	return x * x + y * y + z * z;
}

double vec3::len()
{
	return sqrt(x * x + y * y + z * z);
}

vec3 vec3::vector3FromAngles(double yaw, double pitch)
{
	yaw = deg2rad(yaw);
	pitch = deg2rad(pitch);
	return vec3(-sin(yaw) * cos(pitch), sin(pitch), -cos(yaw) * cos(pitch));
}