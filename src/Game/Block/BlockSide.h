#pragma once

enum BlockSide
{
	TOP = 0,
	BOTTOM,
	NORTH,
	WEST,
	SOUTH,
	EAST
};