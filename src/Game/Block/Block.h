#pragma once

#include <Game\World\World.h>
#include "BlockSide.h"

class Block
{
public:
	const int blockId;
	Block(int id);
	virtual bool shouldRenderSize(World * wld, int x, int y, int z, BlockSide side);
	virtual void render(World * wld, int x, int y, int z, Mesh * mesh);
};