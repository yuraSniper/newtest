#include "Block.h"

Block::Block(int id) : blockId(id)
{
}

bool Block::shouldRenderSize(World * wld, int x, int y, int z, BlockSide side)
{
	switch (side)
	{
		case TOP:
			return wld->getBlockId(x, y + 1, z) == 0;
		case BOTTOM:
			return wld->getBlockId(x, y - 1, z) == 0;
		case NORTH:
			return wld->getBlockId(x, y, z - 1) == 0;
		case WEST:
			return wld->getBlockId(x - 1, y, z) == 0;
		case SOUTH:
			return wld->getBlockId(x, y, z + 1) == 0;
		case EAST:
			return wld->getBlockId(x + 1, y, z) == 0;
	}
}

void Block::render(World * wld, int x, int y, int z, Mesh * mesh)
{
	static Vertex top[] = {
		Vertex(0, 1, 0, 0xffdddddd),
		Vertex(0, 1, 1, 0xffdddddd),
		Vertex(1, 1, 1, 0xffdddddd),
		Vertex(1, 1, 0, 0xffdddddd)
	};
	static Vertex bottom[] = {
		Vertex(0, 0, 1, 0xff999999),
		Vertex(0, 0, 0, 0xff999999),
		Vertex(1, 0, 0, 0xff999999),
		Vertex(1, 0, 1, 0xff999999)
	};
	static Vertex north[] = {
		Vertex(1, 1, 0, 0xff999999),
		Vertex(1, 0, 0, 0xff999999),
		Vertex(0, 0, 0, 0xff999999),
		Vertex(0, 1, 0, 0xff999999)
	};
	static Vertex west[] = {
		Vertex(0, 1, 0, 0xff999999),
		Vertex(0, 0, 0, 0xff999999),
		Vertex(0, 0, 1, 0xff999999),
		Vertex(0, 1, 1, 0xff999999)
	};
	static Vertex south[] = {
		Vertex(0, 1, 1, 0xff999999),
		Vertex(0, 0, 1, 0xff999999),
		Vertex(1, 0, 1, 0xff999999),
		Vertex(1, 1, 1, 0xff999999)
	};
	static Vertex east[] = {
		Vertex(1, 1, 1, 0xff999999),
		Vertex(1, 0, 1, 0xff999999),
		Vertex(1, 0, 0, 0xff999999),
		Vertex(1, 1, 0, 0xff999999)
	};

	if (shouldRenderSize(wld, x, y, z, TOP))
		mesh->addVertex(top, 4);
	if (shouldRenderSize(wld, x, y, z, BOTTOM))
		mesh->addVertex(bottom, 4);
	if (shouldRenderSize(wld, x, y, z, NORTH))
		mesh->addVertex(north, 4);
	if (shouldRenderSize(wld, x, y, z, WEST))
		mesh->addVertex(west, 4);
	if (shouldRenderSize(wld, x, y, z, SOUTH))
		mesh->addVertex(south, 4);
	if (shouldRenderSize(wld, x, y, z, EAST))
		mesh->addVertex(east, 4);
}