#pragma once

#include <Util\Math\vec3.h>

class Entity
{
public:
	vec3 chunkPos;
	vec3 relPos;

	void setPosition(vec3 & newChunkPos, vec3 & newPos);
	void setPosition(int chunkX, int chunkZ, double posX, double posY, double posZ);
};