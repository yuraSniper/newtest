#include "EntityPlayer.h"

vec3 EntityPlayer::getLookVector()
{
	return vec3::vector3FromAngles(lookAngles.y, lookAngles.x);
}

void EntityPlayer::setEyeOffset(vec3 & newPos)
{
	eyeOffset = newPos;
}

void EntityPlayer::setEyeOffset(double posX, double posY, double posZ)
{
	eyeOffset.x = posX;
	eyeOffset.y = posY;
	eyeOffset.z = posZ;
}