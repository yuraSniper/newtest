#pragma once

#include <Util\std.h>
#include "Entity.h"

class EntityPlayer : public Entity
{
public:
	vec3 eyeOffset;
	vec3 lookAngles;

	vec3 getLookVector();

	void setEyeOffset(vec3 & newPos);
	void setEyeOffset(double posX, double posY, double posZ);
};