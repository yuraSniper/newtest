#include "Entity.h"

void Entity::setPosition(vec3 & newChunkPos, vec3 & newPos)
{
	relPos = newPos;
	chunkPos = newChunkPos;
}

void Entity::setPosition(int chunkX, int chunkZ, double posX, double posY, double posZ)
{
	relPos.x = posX;
	relPos.y = posY;
	relPos.z = posZ;

	chunkPos.x = chunkX;
	chunkPos.z = chunkZ;
}