#include "Chunk.h"
#include <Game\Block\Block.h>
#include <Test.h>

Chunk::Chunk(short ax, short az)
{
	xPos = ax;
	zPos = az;

	meshes = new Mesh[meshCount];

	for (int y = 0; y < height / 4; y++)
		for (int pos = 0; pos < size2; pos++)
			blocks[pos | (y << sizePow2)] = 1;

	for (int y = height / 4; y < height; y++)
		for (int pos = 0; pos < size2; pos++)
			blocks[pos | (y << sizePow2)] = 0;
}

void Chunk::setBlockId(short id, short x, short y, short z)
{
	if (x < 0 || x >= size || y < 0 || y >= height || z < 0 || z >= size)
		return;
	blocks[x | (z << sizePow) | (y << sizePow2)] = id;
}

short Chunk::getBlockId(short x, short y, short z)
{
	if (x < 0 || x >= size || y < 0 || y >= height || z < 0 || z >= size)
		return -1;
	return blocks[x | (z << sizePow) | (y << sizePow2)];
}

unsigned int Chunk::getHash()
{
	return getHash(xPos, zPos);
}

unsigned int Chunk::getHash(unsigned short x, unsigned short z)
{
	return (x << 16) | z;
}

Mesh * Chunk::getMesh(short meshId)
{
	return meshes + meshId;
}

void Chunk::updateMesh(short meshId)
{
	static Block * block = new Block(1);

	if (meshId == -1)
	{
		for (int i = 0; i < meshCount; i++)
			updateMesh(i);
		return;
	}
	Mesh & tmp = meshes[meshId];
	tmp.updating = true;
	tmp.clear();
	for (int y = meshId << meshHeightPow; y < ((meshId + 1) << meshHeightPow); y++)
	{
		tmp.yOffset = y;
		for (int x = 0; x < size; x++)
		{
			tmp.xOffset = x;
			for (int z = 0; z < size; z++)
			{
				tmp.zOffset = z;
				if (blocks[x | (z << sizePow) | (y << sizePow2)] != 0)
					block->render(Test::getInstance().world, (xPos << sizePow) | x,
					y, (zPos << sizePow) | z, &tmp);
			}
		}
	}
	tmp.updating = false;
	tmp.updateScheduled = false;
	tmp.generated = true;
}