#pragma once

#include <Core\Render\Mesh.h>

class Chunk
{
public:
	short xPos, zPos;
	static const int sizePow = 4, sizePow2 = sizePow * 2;
	static const int size = 1 << sizePow, size2 = size * size;
	static const int heightPow = 7, height = 1 << heightPow;
	static const int meshHeightPow = 4, meshHeight = 1 << meshHeightPow;
	static const int meshCountPow = heightPow - meshHeightPow, meshCount = 1 << meshCountPow;
private:
	unsigned short blocks[1 << (sizePow + sizePow + heightPow)];
	Mesh * meshes;
public:
	Chunk(short ax, short az);
	
	void setBlockId(short id, short x, short y, short z);
	short getBlockId(short x, short y, short z);
	unsigned int getHash();
	static unsigned int getHash(unsigned short x, unsigned short z);
	Mesh * getMesh(short meshId);
	void updateMesh(short meshId);
};