#pragma once

#include <Util\std.h>
#include "Chunk.h"

class World
{
	struct nd
	{
		short x, z;
		int dist;
	};
	nd * renderArea;
	int renderChunksCount;
	map<unsigned int, Chunk *> chunks;
	void initRender();
public:
	World();
	void setBlockId(short id, int x, int y, int z);
	short getBlockId(int x, int y, int z);
	Chunk * getChunk(short x, short z);
	Chunk * getChunkFromBlock(int x, int z);
	void loadChunk(short x, short z);
	void unloadChunk(short x, short z);
	void renderFromPlayerPos();
};