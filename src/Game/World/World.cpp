#include "World.h"
#include <Test.h>
#include <Core\ThreadOperations\ChunkLoadTask.h>
#include <Core\ThreadOperations\ChunkUpdateTask.h>
#include <Core\ProcessingThread.h>

World::World()
{
	renderArea = nullptr;
	initRender();
}

void World::setBlockId(short id, int x, int y, int z)
{
	Chunk * chunk = getChunkFromBlock(x, z);
	if (chunk != nullptr)
		chunk->setBlockId(id, x & (Chunk::size - 1), y, z & (Chunk::size - 1));
}

short World::getBlockId(int x, int y, int z)
{
	Chunk * chunk = getChunkFromBlock(x, z);
	if (chunk != nullptr)
		return chunk->getBlockId(x & (Chunk::size - 1), y, z & (Chunk::size - 1));
	return -1;
}

Chunk * World::getChunk(short x, short z)
{
	if (chunks.count(Chunk::getHash(x, z)) > 0)
		return chunks.at(Chunk::getHash(x, z));
	return nullptr;
}

Chunk * World::getChunkFromBlock(int x, int z)
{
	return getChunk(x >> Chunk::sizePow, z >> Chunk::sizePow);
}

void World::loadChunk(short x, short z)
{
	if (chunks.count(Chunk::getHash(x, z)) == 0)
	{
		Chunk * chunk = new Chunk(x, z);
		chunks.insert(make_pair(chunk->getHash(), chunk));
	}
}

void World::unloadChunk(short x, short z)
{
	Chunk * chunk = getChunk(x, z);
	if (chunk != nullptr)
	{
		chunks.erase(chunk->getHash());
	}
	delete chunk;
}

void World::renderFromPlayerPos()
{
	static ProcessingThread * thread = (ProcessingThread *)Test::getInstance().miscThread;
	EntityPlayer * player = Test::getInstance().player;
	for (int i = 0; i < renderChunksCount; i++)
	{
		Chunk * chunk = getChunk(renderArea[i].x + player->chunkPos.x, renderArea[i].z + player->chunkPos.z);
		if (chunk != nullptr)
		{
				for (int j = 0; j < Chunk::meshCount; j++)
				{
					Mesh * mesh = chunk->getMesh(j);
					if (!mesh->updateScheduled)
					{
						if (!mesh->generated)
						{
							mesh->updateScheduled = true;
							thread->threadQueue->push(new ChunkUpdateTask(renderArea[i].x + player->chunkPos.x,	renderArea[i].z + player->chunkPos.z, j), 3);
						}
						else
						{
							glPushMatrix();
							glTranslated((int)(renderArea[i].x) << 4,
								0, (int)(renderArea[i].z) << 4);
							mesh->draw();
							glPopMatrix();
						}
					}
				}
		}
		else
		{
			thread->threadQueue->push(new ChunkLoadTask(renderArea[i].x + player->chunkPos.x, renderArea[i].z + player->chunkPos.z), 1);
		}
	}
}

void World::initRender()
{
	renderChunksCount = ((Test::renderChunkRadius << 1) - 1);
	renderChunksCount *= renderChunksCount;
	if (renderArea != nullptr)
		delete renderArea;
	renderArea = new nd[renderChunksCount];

	short orgX = Test::getInstance().player->chunkPos.x;
	short orgZ = Test::getInstance().player->chunkPos.z;

	short k = 0;

	for (short i = -Test::renderChunkRadius + 1; i < Test::renderChunkRadius; i++)
		for (short j = -Test::renderChunkRadius + 1; j < Test::renderChunkRadius; j++)
		{
			renderArea[k].x = i;
			renderArea[k].z = j;
			renderArea[k].dist = (i - orgX) * (i - orgX) + (j - orgZ) * (j - orgZ);
			k++;
		}

	qsort(renderArea, renderChunksCount, sizeof(nd), [](const void * a,
			const void * b)	-> int {return ((nd *)a)->dist - ((nd *)b)->dist;});
}