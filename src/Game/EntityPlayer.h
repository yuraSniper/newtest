#pragma once

#include <Util\std.h>
#include <Util\Math\vec3.h>

class EntityPlayer
{
public:
	vec3 pos;
	vec3 eyeOffset;
	vec3 lookAngles;

	vec3 getLookVector();

	void setPosition(vec3 & newPos);
	void setPosition(double posX, double posY, double posZ);

	void setEyeOffset(vec3 & newPos);
	void setEyeOffset(double posX, double posY, double posZ);
};