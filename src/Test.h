#pragma once

#include <Core\Window.h>
#include <Core\Input.h>
#include <Core\IThread.h>
#include <Util\std.h>
#include <Util\Util.h>
#include <Game\Entity\EntityPlayer.h>
#include <Game\World\World.h>

class Test
{
	bool working, prevMouse;
	void mainLoop();
	void handleInput();
public:
	IThread * renderThread, * miscThread;
	World * world;
	EntityPlayer * player;
	Window * mainWnd;
	Input * input;
	bool trapMouse;
	static const int renderChunkRadius = 4;

	static Test & getInstance();
	Test();
	void shutdown();
};